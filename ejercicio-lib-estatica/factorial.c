#include "libstop.h"
#include <stdio.h>

int main() {
    int n;
    int f;
    
    printf("Ingrese el dato:\t \n");
    scanf("%d",&n);
    
    if (n<0){
        printf("Error ingrese un valor correcto");
    
    }else{
        f=factorial(n);
        
        printf("El factorial del numero %d!=%d\n",n,f);
    }
return 0;
}

long factorial (int n){
    if(n==0 || n==1){
        return 1;
    }else if(n>2) {
        int factorial=1;
        while (n>1)
        {
         factorial*=n; 
         n --;  
        }
        return factorial;

        }   
    }